$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 3000
    });
    $('#contacto').on('show.bs.modal', function (e) {
        console.log('modal mostrandose');
        $('#modalBtn').removeClass('btn-outline-success');
        $('#modalBtn').addClass('btn-primary');
        $('#modalBtn').prop('disabled', true);
    })
    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('modal mostrado');
    })
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('modal ocultandose');
    })
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('modal ocultado');
        $('#modalBtn').prop('disabled', false)
    })
});